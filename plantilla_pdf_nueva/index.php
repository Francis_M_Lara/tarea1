<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>plantilla_pdf_nueva</title>
  <style> #map {width: 40%; height: 200px;} </style> 
  
</head>
<body>
  <input type="text" name="latitud" id="latitud">
  <input type="text" name="longitud" id="longitud">
  <div id = 'map'></div>

    <button id="btnCapturar">Tomar captura</button>
    <button id="btn">Mostrar</button>
    <form action="index_pdf.php" method="post">
      <button>Ver PDF</button>
    </form>

  <div id="contenedorCanvas" style="border: 1px solid red;"></div>
    
  <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
  <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDbEx0w87e9r_5NsxE4A4S5olFa1nts4J0&callback=initmap&libraries=geometry&v=weekly"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/html2canvas@1.0.0-rc.1/dist/html2canvas.min.js"></script>

  <script src="js/main.js"></script>
</body>
</html>