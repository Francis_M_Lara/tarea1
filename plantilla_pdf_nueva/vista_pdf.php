<?php

function getPlantilla($resultado){
    $plantilla = '<body>

        <header class="clearfix">
            <div id="logo">
                <img src="img/favicon.png" style="height:70px;width:auto;">
            </div>
            <div id="company">
                <h2>Oportucredit, S.A</h2>
                <div>Direccion de la empresa.</div>
            </div>       
        </header>
        <strong>Planificacion de Actividades<strong>
        <hr>
        <main>
            <div id="details" class="clearfix">
                <p><strong>Titulo de la Actividad:</strong> Auditoria de Campo en Ruta 9 MNG.</p>

                <strong>Descripcion de la Actividad:</strong>

                <p>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, 
                when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, 
                but also the leap into electronic typesetting, remaining essentially unchanged. 
                It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, 
                and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                
                <img src="'.$resultado['foto'].'" style="height:155px;width:auto;">
            
                </div>
            <table border="0" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <th>Fecha de Inicio de la Actividad</th>
                    <th>Fecha de Finalizacion de la Actividad</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>11/12/2020 10:45 AM</td>
                    <td>14/12/2020 11:45 PM</td>
                </tr>
                
            </tbody>
            </table>
            
            <div id="details" class="clearfix">
                <p><strong>Estado: </strong>Cumplida</p>

                <strong>Observaciones:</strong>

                <p>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, 
                when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, 
                but also the leap into electronic typesetting, remaining essentially unchanged. 
                It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, 
                and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>        
            </div>
        </main>

        <aside class="aside1">
            <hr class="hr">
            <p>Creado Por: Jean Carlos</p>
            <p>Departamento: Gte. TI</p>
        </aside>

        <article class="aside2">
            <hr class="hr">
            <p>Asignado A: Fernando</p>
            <p>Departamento: Pasante TI</p>
            
        </article>
            
        <footer>
            Actividad Creada: 10/02/2020 11:00PM
        </footer>
</body>';

    return $plantilla;
}