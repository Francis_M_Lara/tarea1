let lat, long;
$(document).ready(function(){
    document.getElementById("btn").addEventListener("click", function(){
      lat = $("#latitud").val();
      long = $("#longitud").val();
      initialize(parseFloat(lat),parseFloat( long));
    });

    document.getElementById("btnCapturar").addEventListener("click", toCanvas);
});

function initialize(lat, lng) {
  const coords = { lat: lat, lng: lng };
  const map = new google.maps.Map(document.getElementById("map"), {
    zoom: 13,
    center: coords,
    disableDefaultUI: true,
});
const contentString =
    '<b>Hola!</b>'+
    '<p>Este es el lugar de la actividad</p>';
  const infowindow = new google.maps.InfoWindow({
    content: contentString,
  });
  const marker = new google.maps.Marker({
    position: coords,
    map,
    title: "Actividad",
  });
  marker.addListener("click", () => {
    infowindow.open(map, marker);
  });

}

function toCanvas() {

let mapContainer = document.getElementById("map");
let renderingArea = document.getElementById("contenedorCanvas");

var transform = $(".gm-style>div:first>div:first>div:last>div").css("transform");
console.log(transform);
var comp = transform.split(",") //split up the transform matrix
var mapleft = parseFloat(comp[4]) //get left value
var maptop = parseFloat(comp[5]) //get top value
$(".gm-style>div:first>div:first>div:last>div").css({
    "transform": "none",
    "left": mapleft,
    "top": maptop,
});
html2canvas(mapContainer, {
 useCORS: true,
 width: mapContainer.offsetWidth,
 height: mapContainer.offsetHeight,
}).then(function(canvas) {
    renderingArea.appendChild(canvas);
  $(".gm-style>div:first>div:first>div:last>div").css({
     left: 0,
     top: 0,
     "transform": transform
  });
    // Convertir la imagen a Base64
  let imagenComoBase64 = canvas.toDataURL();
  // Codificarla, ya que a veces aparecen errores si no se hace
  imagenComoBase64 = encodeURIComponent(imagenComoBase64);
  // La carga útil como JSON
  const payload = {
    "captura": imagenComoBase64,
    "by": "localhost",
    // Aquí más datos...
  };
  // Aquí la ruta en donde enviamos la foto. Podría ser una absoluta o relativa
  const ruta = "./bd/crud.php";
  fetch(ruta, {
    method: "POST",
    body: JSON.stringify(payload),
    headers: {
      "Content-type": "application/x-www-form-urlencoded",
    }
  })
    .then(resultado => {
      // A los datos los decodificamos como texto plano
      return resultado.text()
    })
    .then(nombreDeLaFoto => {
      // nombreDeLaFoto trae el nombre de la imagen que le dio PHP
      console.log({ nombreDeLaFoto });
      //alert(`Guardada como ${nombreDeLaFoto}`);
    });
   
});

}