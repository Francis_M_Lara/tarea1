<?php

require_once('vendor/autoload.php');
require_once('vista_pdf.php');

$css =file_get_contents('css/styles.css');

include_once 'bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();
$id=$conexion->query("SELECT MAX(codigo) AS id FROM foto")->fetch();
$codigo_foto = $id['id'];
$resultado=$conexion->query("SELECT * FROM foto WHERE codigo='$codigo_foto'")->fetch();

$mpdf = new \Mpdf\Mpdf([]);
$plantilla = getPlantilla($resultado);
   
$mpdf->WriteHtml($css, \Mpdf\HTMLParserMode::HEADER_CSS);
$mpdf->WriteHtml($plantilla, \Mpdf\HTMLParserMode::HTML_BODY);
$mpdf->Output();