<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Fomulario Llamadas</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/styles.css">
  <!--datables CSS básico-->
  <link rel="stylesheet" type="text/css" href="dist/datatables/datatables.min.css"/>
  <!--datables estilo bootstrap 4 CSS-->  
  <link rel="stylesheet"  type="text/css" href="dist/datatables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css">   
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  
</head>
<body class="hold-transition layout-top-nav">
  
<div class="wrapper">

  <section class="content">
      <div class="container-fluid">
        <div class="row">
         
          <div class="col-md-12">
            
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">AÑADIR LLAMADAS</h3>
              </div>
             
              <form id="formLlamadas">
                <div class="card-body">    
                  <div class="row">
                      <div class="form-group mb-5 col-4">
                          <label>Sucursal</label>
                            <select id="sucursal" name="sucursal" class="form-control custom-select">
                              <option value="" selected>Seleccione un opcion</option>
                              <?php include_once 'bd/conexion.php'; $objeto = new Conexion(); $conexion = $objeto->Conectar();
                                  $consulta = "SELECT * FROM sucursal"; $resultado = $conexion->prepare($consulta); $resultado->execute(array());
                                    while($row=$resultado->fetch(PDO::FETCH_ASSOC)){
                                      echo "<option value=".$row['id'].">".$row['nombre']."</option>";   
                                    }
                              ?>
                            </select>
                      </div>
                    
                      <div class="form-group mb-5 col-4">
                        <label>Rutas</label>
                          <select id="rutas" name="rutas" class="form-control custom-select">
                            <option value="" selected>Seleccione un opcion</option>
                            
                          </select>
                      </div>

                      <div class="form-group mb-5 col-4">
                          <label>Clientes</label>
                            <select id="cliente" name="cliente" class="form-control custom-select">
                              <option value="" selected>Seleccione un opcion</option>
                             
                            </select>
                          </div>
                      </div>
                       <div id="text" class="form-group mb-5">
                         <label>Comentario</label>
                          <textarea class="form-control" name="txt" id="txt" rows="3"></textarea>
                       </div>
                 
                      <button type="submit" id="btnRegistrar" class="btn btn-success">Registrar una Llamada</button>
                     </form>   
                     <span id="btn"></span>
                      <hr>
                      
                      <div class="row">
                        <div class="col-lg-12">
                         <div class="table-responsive">        
                            <table id="tablaLlamadas" class="table table-striped table-bordered table-condensed" style="width:100%" >
                              <thead class="text-center">
                                <tr>
                                   <th>#</th>
                                   <th>Comentarios</th>                               
                                   <th>Horario Llamada</th>  
                                   <th>Cliente Llamado</th>
                                   
                                </tr>
                             </thead>
                             <tbody>                 
                             </tbody>        
                           </table>               
                          </div>
                        </div>
                      </div> 
                      
                      <div class="modal fade" id="modalCRUD" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                                    </button>
                                </div>   
                                <div class="modal-body">
                                  <div class="table-responsive">        
                                      <table id="tablaModal" class="table table-striped table-bordered table-condensed" style="width:100%" >
                                         <thead class="text-center">
                                           <tr class="bg-info">
                                           <?php $consulta = "SELECT sucursal.nombre, ruta.localizacion FROM sucursal
                                                  INNER JOIN ruta ON sucursal.id = ruta.id_sucursal";
                                                  $resultado = $conexion->prepare($consulta);$resultado->execute(array());$cant_rutas = $resultado->rowCount();
                                            while($row=$resultado->fetch(PDO::FETCH_ASSOC)){
                                                   echo" <td>".$row['localizacion']."</td>";
                                            } ?>
                                           </tr>
                                         </thead>
                                         <tbody>
                                          <tr>
                                            <?php
                                              $array_llamadas = array();for($i = 1; $i < $cant_rutas+1; $i++){
                                                  $consulta2= "SELECT sucursal.nombre, ruta.localizacion, cliente.nombres, cliente.apellidos, llamada.comentario FROM sucursal
                                                  INNER JOIN ruta ON sucursal.id = ruta.id_sucursal 
                                                  INNER JOIN cliente ON ruta.id = cliente.id_ruta
                                                  INNER JOIN llamada ON cliente.id = llamada.id_cliente where id_ruta=$i";
                                              
                                                  $resultado2 = $conexion->prepare($consulta2);$resultado2->execute(array());$num_filas = $resultado2->rowCount();$array_llamadas [$i] = $num_filas;
                                                }
                                                foreach($array_llamadas as $val){
                                                  echo "<td>".$val."</td>";
                                              }  
                                            ?>
                                          </tr>
                                         </tbody>        
                                       </table>
                                   </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-dark" data-dismiss="modal">Salir</button>
                                </div> 
                            </div>
                        </div>
                    </div>  
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button id="btnHistorial" type="submit" class="btn btn-info">Ver historial</button>
                </div>
            </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
  </section>
    
  <!-- /.content-wrapper -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- datatables JS -->
<script type="text/javascript" src="dist/datatables/datatables.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>

<script src="https://unpkg.com/imask"></script>

<script src="js/main.js"></script>
</body>
</html>
