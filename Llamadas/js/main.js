$(document).ready(function(){
    opcion = 5;
    tablaLlamadas = $('#tablaLlamadas').DataTable({
        "ajax":{            
            "url": "bd/crud.php", 
            "method": 'POST',
            "data":{opcion:opcion}, 
            "dataSrc":""
        },
        "columns":[
            {"data": "id"},
            {"data": "comentario"},
            {"data": "programado_llamar"},
            {"data": "id_cliente"}
        ],
    });    
    
    $("#sucursal").change(function(){
        opcion = 2;
        var cod = document.getElementById("sucursal").value;

        $.ajax({
            type: 'post',
            url: 'bd/crud.php',
            data: {sucursal:cod, opcion:opcion},
            success: function (response) {
             $("#rutas").html(response); 
            }
        });
    });

    $("#rutas").change(function(){
        opcion = 3;
        var cod = document.getElementById("rutas").value;

        $.ajax({
            type: 'post',
            url: 'bd/crud.php',
            data: {rutas:cod, opcion:opcion},
            success: function (response) {
             $("#cliente").html(response); 
            }
        });
    });

    $("#formLlamadas").submit(function(e){
        e.preventDefault();
        opcion = 4;
        var hoy = new Date();
        var fecha = hoy.getDate()+"/"+(hoy.getMonth()+1)+"/"+hoy.getFullYear();
        var hora = hoy.getHours()+":"+hoy.getMinutes();
        var fechaYhora = fecha +' '+ hora;

        var texto = $("#txt").val();
        comentario = $.trim(texto).replace(/\s(?=\s)/g,'');
        id_cliente = document.getElementById("cliente").value;
        programado_llamar = $.trim(fechaYhora);
        console.log(comentario + " "+ id_cliente + " "+programado_llamar);
        $.ajax({
            url: "bd/crud.php",
            type: "POST",
            datatype:"json",    
            data:  {comentario:comentario, programado_llamar:programado_llamar, id_cliente:id_cliente, opcion:opcion},    
            success: function(data) {
              tablaLlamadas.ajax.reload(null, true);
              $("#cliente").find('option[value='+id_cliente+']').remove();
              $("#txt").val(' '); 
            }
        });	
    });
    $("#btnHistorial").click(function(e){
        e.preventDefault();
        $('#modalCRUD').modal('show');
        
    });
});