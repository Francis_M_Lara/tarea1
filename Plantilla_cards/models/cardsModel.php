<?php

function nombre_card(){
    try{
        $base = new PDO('mysql:host=localhost; dbname=prueba', 'root', '');
        $base->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $base->exec("SET CHARACTER SET UTF8");
        
        $tam_pagina = 9;
        if(isset($_GET["pagina"])){
            if($_GET["pagina"]==1){
                header("Location: cards.php");
            }else{
                $pagina=$_GET["pagina"];
            }
        }else{
            $pagina =1;
        }    
        
        $empezar_desde = ($pagina-1)*$tam_pagina;
        $sql_total = "SELECT registro.nombre, registro.apellidos, registro.correo, 
        registro.cedula, registro.telefono, registro.sexo, registro.img, tbl_prueba_fernando.study,
        tbl_prueba_fernando.experience, tbl_prueba_fernando.reference FROM registro INNER JOIN tbl_prueba_fernando ON registro.id = tbl_prueba_fernando.id_deta_can";
    
        $resultado = $base->prepare($sql_total);
        $resultado->execute(array());
        $num_filas = $resultado->rowCount();
        $total_paginas = ceil($num_filas/$tam_pagina);
    
        $resultado->closeCursor();
        
        if(!isset($_POST['buscar'])){
            $_POST['buscar'] = "";
            $buscar = $_POST['buscar'];
        }
        $buscar = $_POST['buscar'];
    
        $sql_limite = "SELECT registro.nombre, registro.apellidos, registro.correo, 
        registro.cedula, registro.telefono, registro.sexo, registro.img, tbl_prueba_fernando.study,
        tbl_prueba_fernando.experience, tbl_prueba_fernando.reference FROM registro INNER JOIN tbl_prueba_fernando 
        ON registro.id = tbl_prueba_fernando.id_deta_can WHERE tbl_prueba_fernando.experience LIKE '%".$buscar."%' LIMIT ".$empezar_desde.",".$tam_pagina."";
    
        $resultado = $base->prepare($sql_limite);
        $resultado->execute(array());
     
    }catch(PDOException $e){
        die('Error: '. $e->GetMessage());
    }

    return $resultado;
}

function paginacion(){
    try{
        $base = new PDO('mysql:host=localhost; dbname=prueba', 'root', '');
        $base->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $base->exec("SET CHARACTER SET UTF8");

        $tam_pagina = 9;
        if(isset($_GET["pagina"])){
            if($_GET["pagina"]==1){
                header("Location: cards.php");
            }else{
                $pagina=$_GET["pagina"];
            }
        }else{
            $pagina =1;
        }    
        
        $empezar_desde = ($pagina-1)*$tam_pagina;
        $sql_total = "SELECT registro.nombre, registro.apellidos, registro.correo, 
        registro.cedula, registro.telefono, registro.sexo, registro.img, tbl_prueba_fernando.study,
        tbl_prueba_fernando.experience, tbl_prueba_fernando.reference FROM registro INNER JOIN tbl_prueba_fernando ON registro.id = tbl_prueba_fernando.id_deta_can";
    
        $resultado = $base->prepare($sql_total);
        $resultado->execute(array());
        $num_filas = $resultado->rowCount();
        $total_paginas = ceil($num_filas/$tam_pagina);
    
        $resultado->closeCursor();
    
        $sql_limite = "SELECT registro.nombre, registro.apellidos, registro.correo, 
        registro.cedula, registro.telefono, registro.sexo, registro.img, tbl_prueba_fernando.study,
        tbl_prueba_fernando.experience, tbl_prueba_fernando.reference FROM registro INNER JOIN tbl_prueba_fernando ON registro.id = tbl_prueba_fernando.id_deta_can LIMIT ".$empezar_desde.",".$tam_pagina."";
    
        $resultado = $base->prepare($sql_limite);
        $resultado->execute(array());
    
         
    }catch(PDOException $e){
        die('Error: '. $e->GetMessage());
    }

    return $total_paginas;
}
?>