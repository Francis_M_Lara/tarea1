<?php

class ModelCards{

    static public function ContarFilas(){
        try{
            $base = new PDO('mysql:host=localhost; dbname=prueba', 'root', '');
            $base->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $base->exec("SET CHARACTER SET UTF8");
            
            $sql_total = $base->prepare("SELECT registro.nombre, registro.apellidos, registro.correo, 
            registro.cedula, registro.telefono, registro.sexo, registro.img, tbl_prueba_fernando.study,
            tbl_prueba_fernando.experience, tbl_prueba_fernando.reference FROM registro INNER JOIN tbl_prueba_fernando ON registro.id = tbl_prueba_fernando.id_deta_can");

            $sql_total -> execute();
            $sql_total->fetchAll();
            $num_filas = $sql_total->rowCount();
            return $num_filas;
            $sql_total->close();
            $sql_total = null;
        }catch(PDOException $e){
            die('Error: '. $e->GetMessage());
        }
        
    }

    static public function GenerateCard($buscar, $empezar_desde, $tam_pagina){
        $stmt=$base->prepare("SELECT registro.nombre, registro.apellidos, registro.correo, 
        registro.cedula, registro.telefono, registro.sexo, registro.img, tbl_prueba_fernando.study,
        tbl_prueba_fernando.experience, tbl_prueba_fernando.reference FROM registro INNER JOIN tbl_prueba_fernando 
        ON registro.id = tbl_prueba_fernando.id_deta_can WHERE tbl_prueba_fernando.experience LIKE '%:buscar%' LIMIT :empezar_desde,:tam_pagina");

        $stmt->bindParam(":buscar", $buscar, PDO::PARAM_STR);
        $stmt->bindParam(":empezar_desde", $empezar_desde, PDO::PARAM_STR);
        $stmt->bindParam(":tam_pagina", $tam_pagina, PDO::PARAM_STR);
        $stmt->execute(array());
        return $stmt;
        $stmt->close();
        $stmt=null;
    }
}