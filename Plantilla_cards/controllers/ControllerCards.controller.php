<?php

// class MyClass {
//     const CONST_VALUE = 'Un valor constante';
// }

// $classname = 'MyClass';
// echo $classname::CONST_VALUE; // A partir de PHP 5.3.0

// echo MyClass::CONST_VALUE;
include "models/ModelCards.model.php";
class ControllerCards{
    static public function Paginacion(){
        $tam_pagina = 9;
        if(isset($_GET["pagina"])){
            if($_GET["pagina"]==1){
                header("Location: cards.php");
            }else{
                $pagina=$_GET["pagina"];
            }
        }else{
            $pagina =1;
        }    
        $empezar_desde = ($pagina-1)*$tam_pagina;
        
        $num_filas = ModelCards::ContarFilas();
        $total_paginas = ceil($num_filas/$tam_pagina);

        if(!isset($_POST['buscar'])){
            $_POST['buscar'] = "";
            $buscar = $_POST['buscar'];
        }
        $buscar = $_POST['buscar'];
        $resultado = ModelCards::GenerarCards($buscar, $empezar_desde, $tam_pagina);
        return $resultado;
    }
    
}